<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Admin extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'admin/admin/Admin_service' );
		include_once './application/objects/Response.php';
		$this->load->helper ( 'auth' );
		$this->load->library ( 'form_validation' );
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('excel');
		/*
		 * $this->load->library('Datatables');
		 * $this->load->library('table');
		 */
	}
	public function index() {
		if (is_loggedin ()) {
			redirect ( 'admin/dashboard' );
		} else {
			$this->load->view ( 'admin/login' );
		}
	}
	public function dashboard() {
		if (is_loggedin ()) {
			$data ['metaData'] = 'yes';
			$data ['title'] = 'LMS | Dashboard';
			$data ['keywords'] = '';
			$data ['description'] = '';
			$this->template->load ( 'admin/dashboard', $data );
			// $this->load->view('admin/dashboard');
		} else {
			redirect ( 'admin' );
		}
	}
	public function forgot_password() {
		$this->load->view ( 'admin/forgot_password' );
	}
	public function reset_password() {
		$email = $this->input->post ( 'email' );
		
		$check = $this->Admin_service->checkEmail ( $email );
		
		if ($check) {
			
			$random_code = mt_rand ( 100000, 999999 ); // echo $random_code;
			$update = $this->Admin_service->updatePassword ( $random_code, $email );
			
			if ($update) {
				
				$response ['status'] = 1;
				$response ['msg'] = "Your new password has been sent to your email.";
			}
			
			$this->send_mail ( $random_code, $email );
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Email id doesn't exist";
		}
		
		echo json_encode ( $response );
		
		// print_r($check); die();
		
		// $this->load->view('admin/forgot_password');
	}
	function send_mail($random_code, $email) {
		error_reporting ( 0 );
		// print_r($value); die();
		// $subject = $value['subject'];
		
		$message = "<b>Hi, </b>" . '</br>';
		$message .= '<b>Your new temporory password is ' . $random_code . ' </b>' . '</br>';
		$message .= '<b>After login you can change your password</b>' . '</br>';
		
		// print_r($subject);
		// print_r($message); //die();
		
		$this->load->library ( 'email' );
		$this->email->set_mailtype ( 'html' );
		$this->email->from ( 'contact@lms.com' );
		$this->email->to ( $email );
		$this->email->subject ( 'forgot Password' );
		$this->email->message ( $message );
		$this->email->send ();
	}
	public function profile() {
		$userid = $this->session->userdata ( 'id' );
		$data ['user'] = $this->Admin_service->GetUserProfile ( $userid );
		// print_r($userid); die();
		// $this->load->view('admin/profile');
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/profile', $data );
	}
	public function update_profile() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' ); // print_r($data); die();
		$update = $this->Admin_service->update_profile ( $data, $userid );
		
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your Profile has been successfully updated";
			// unset($this->input->post());
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}
	public function change_password() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/change_password', $data );
	}
	public function update_password() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' );
		if ($data ['password'] != '' || $data ['repassword'] != '') {
			if ($data ['password'] === $data ['repassword']) {
				unset ( $data ['repassword'] );
				$data ['password'] = md5 ( $data ['password'] );
				$update = $this->Admin_service->update_password ( $data, $userid );
				if ($update) {
					$response ['status'] = 1;
					$response ['msg'] = "Your Password has been successfully changed";
				}
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Password doesn't match , please enter same password";
			}
		} 

		else {
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the Password";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: view_roles
	 *         Description: view Roles
	 */
	public function view_roles() {
		$data ['user'] = $this->Admin_service->getRoles ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_roles', $data );
	}
	public function getRoles() {
		$details ['data'] = $this->Admin_service->getRoles ();
		echo json_encode ( $details );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: view_privileges
	 *         Description: view privileges
	 */
	/*
	 * public function view_privileges() {
	 * $data ['user'] = $this->Admin_service->getPrivileges ();
	 * // print_r($userid); die();
	 * // $this->load->view('admin/profile');
	 * $data ['metaData'] = 'yes';
	 * $data ['title'] = 'LMS | Dashboard';
	 * $data ['keywords'] = '';
	 * $data ['description'] = '';
	 * // print_r($data); die();
	 * $this->template->load ( 'admin/view_privileges', $data );
	 * }
	 */
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: edit_role
	 *         Description: edit role
	 */
	public function edit_role() {
		$id = $this->uri->segment ( 3 );
		$data ['user'] = $this->Admin_service->editRoles ( $id );
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/edit_role', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: edit_privileges
	 *         Description: edit privileges
	 */
	public function edit_privileges() {
		$id = $this->uri->segment ( 3 );
		// print_r($id); die();
		$data ['user'] = $this->Admin_service->editPrivileges ( $id );
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		// print_r($data); die();
		$this->template->load ( 'admin/edit_privilege', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: edit_privileges
	 *         Description: edit privileges
	 */
	public function update_role() {
		$data = $this->input->post ( 'user' );
		$update = $this->Admin_service->update_role ( $data );
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully updated";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: edit_privileges
	 *         Description: edit privileges
	 */
	public function update_privilege() {
		$data = $this->input->post ( 'user' );
		// $id = $data['id'];
		// print_r($id); die();
		$update = $this->Admin_service->update_privilege ( $data );
		
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your privilege has been successfully updated";
			// redirect()
			// unset($this->input->post());
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: edit_privileges
	 *         Description: edit privileges
	 */
	public function add_role() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/add_role', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: edit_privileges
	 *         Description: edit privileges
	 */
	public function new_role() {
		$data = $this->input->post ( 'user' );
		// print_r($data); die();
		
		if ($data ['title'] == "" || $data ['description'] == "") {
			
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the title or description";
		} else {
			$insert = $this->Admin_service->new_role ( $data );
			if ($insert) {
				$response ['status'] = 1;
				$response ['msg'] = "You insert new role successfully";
				// unset($this->input->post());
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong , please try again";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: edit_privileges
	 *         Description: edit privileges
	 */
	public function add_privilege() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/add_privilege', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 29th Nov 2016
	 *         Method: edit_privileges
	 *         Description: edit privileges
	 */
	public function new_privilege() 

	{
		$data = $this->input->post ( 'user' );
		//print_r($data); die();
		if ($data ['title'] == "" || $data ['description'] == "") {
			
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the title or description";
		} else {
			$insert = $this->Admin_service->new_privilege ( $data );
			if ($insert) {
				$response ['status'] = 1;
				$response ['msg'] = "You insert new privilege successfully";
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong , please try again";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 30th Nov 2016
	 *         Method: add user
	 *         Description: add user
	 */
	public function add_user() 

	{
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/addUser', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 30 Nov 2016
	 *         Method: new_user
	 *         Description: new_user
	 */
	public function new_user() 

	{
		$data = $this->input->post ( 'user' );
		/*
		 * $this->form_validation->set_rules ( 'firstname', 'firstname', 'trim|required' );
		 * $this->form_validation->set_rules ( 'lastname', 'lastname', 'trim|required' );
		 * $this->form_validation->set_rules ( 'email', 'email', 'trim|required|valid_email' );
		 * $this->form_validation->set_rules ( 'gender', 'gender', 'trim|required' );
		 * $this->form_validation->set_rules ( 'dob', 'dob', 'trim|required' );
		 * $this->form_validation->set_rules ( 'password', 'password ', 'trim|required|md5' );
		 * $this->form_validation->set_rules ( 'primaryContact', 'primaryContact', 'trim|required' );
		 * $this->form_validation->set_rules ( 'address', 'address', 'trim|required' );
		 * $this->form_validation->set_rules ( 'city', 'city', 'trim|required' );
		 * $this->form_validation->set_rules ( 'role_Id', 'role_Id', 'trim|required' );
		 * $this->form_validation->set_rules ( 'status', 'status', 'trim|required' );
		 *
		 * if ($this->form_validation->run () == TRUE) {
		 */
		//die();
		$addUser = $this->Admin_service->new_user ( $data );
		if ($addUser != NULL) {
			$response ['status'] = 1;
			$response ['msg'] = "You insert new user successfully";
			$response ['userId'] = $addUser;
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
		/* }else{echo "something wrong";} */
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 1 Dec 2016
	 *         Method: picture
	 *         Description: picture
	 */
	public function picture() 

	{ $data1 = $this->input->post ( 'user' );
	
		$config ['upload_path'] = './uploads';
		$config ['allowed_types'] = 'gif|jpg|png';
		$config ['max_size'] = 1024;
		$config ['max_width'] = 1024;
		$config ['max_height'] = 768;
		$this->load->library ( 'upload', $config );
		// $data = $_FILES;
		$sizeImage = getimagesize ( $_FILES ['fileToUpload'] ['tmp_name'] );
		
		$width = $sizeImage [0];
		$height = $sizeImage [1];
		
		if ($width > 1024 && $height > 768) {
			
			$data = 'you upload size greater than allowed value';
			unlink ( $_FILES ['fileToUpload'] ['tmp_name'] );
			echo $data;
		} else {
			if (! $this->upload->do_upload ( 'fileToUpload' )) {
				
				// $error = array('error' => $this->upload->display_errors());
				// echo "somethning is going wrong";
				$data = "somethning is going wrong";
			} else {
				$pic = array (
						'upload_data' => $this->upload->data () 
				);
				
			}
		}
		echo json_encode ( $pic );
	}
	public function view_privileges() {
		$data ['user'] = $this->Admin_service->getPrivileges ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_privileges', $data );
	}
	public function getPrivileges() {
		$details ['data'] = $this->Admin_service->getPrivileges ();
		echo json_encode ( $details );
	}
	
	
	public function view_user() {
		$data ['user'] = $this->Admin_service->getUserDetails ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_user', $data );
	}
	public function getUserDetails() {
		$details ['data'] = $this->Admin_service->getUserDetails ();
		echo json_encode ( $details );
	}
	public function view_image() {
		$config = array();
		$count=$this->Admin_dao->count();
		$config['base_url'] = base_url() . "admin/view_image";
		$config['total_rows'] = $count;
		$config['per_page'] = 8;//data showing perpage
		 $config["uri_segment"] = 3;
		 $choice = $config["total_rows"] / $config["per_page"];
		 $var=$config['per_page'];
		// print_r($count); die();
		 $config["num_links"] = round($choice);
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		 $data ['files'] = $this->Admin_service->getImage($var,$page);
         $data["links"] = $this->pagination->create_links();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_image', $data );
	}
	public function upload_image()
	{ 
		$desc = $_REQUEST['description'];
		//print_r($desc);
	//	echo json_encode( $desc);
		//die();
	$response = array();
    $file_element_name = 'fileToUpload';
  
				$uploadPath = 'uploads/files/';
				$config['upload_path'] = $uploadPath;
				$config['allowed_types'] = 'gif|jpg|png';
				$this->load->library('upload', $config);
				//$this->upload->initialize($config);
				if (!$this->upload->do_upload($file_element_name))
		        {
		            $response['status'] = 2;
		              $response['msg'] = $this->upload->display_errors('', '');
		             
		           
		        }
		        else
		        {
		            $data = $this->upload->data();
		            //print_r($data['file_name']); 
		            $file_id = $this->Admin_service->addImage($data['file_name'],$desc);
		            
		            if($file_id)
		            {
		                $response['status'] = 1;
		                $response['msg']  = "File successfully uploaded";
		               
		            }
		            else
		            {
		                unlink($data['full_path']);
		                 $response['status']  = 0;
		                $response['msg']  = "Something went wrong when saving the file, please try again.";
		                
		            }
		        }
	
			echo json_encode( $response);
}
}
