<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Api_dao extends CI_Model {
	
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
		date_default_timezone_set('Asia/Calcutta'); 
		$this->load->helper('date');
	    //echo date('Y-m-d H:i:s'); die();
	}
	
	/**
	 * @author : Anjani Kr. Gupta
	 * @method : isUserExist
	 * @Desc : check user exist or not 
	 * @return : dealers list
	 * Date: 10th Nov 2016
	 */
	public function isUserExist($email) {
		$response = new Response ();
		try {
			$query = "SELECT user.*, roles.title role, branch.title branch, (SELECT b.title FROM m_branch b WHERE b.id=branch.parent) cluster FROM user user INNER JOIN m_roles roles ON user.role_id=roles.id
						LEFT JOIN m_branch branch ON branch.id = user.branch WHERE user.email LIKE ".$this->db->escape($email);
			$result = $this->db->query ( $query );
			
			if ($result->num_rows () > 0) {
				$response->setStatus ( 1 );
				$response->setMsg ( "User Exist." );
				$response->setObjArray ( $result->result() );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "User does not Exist." );
				$response->setObjArray ( NULL );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 * @author : Anjani Kr. Gupta
	 * @method : updateDeviceData
	 * @Desc : update device data
	 * @return : true/false
	 * Date: 10th Nov 2016
	 */
	public function updateDeviceData($userId,$deviceType,$deviceId,$fcmRegId) {
			$query = "SELECT * FROM device_info WHERE deviceId LIKE ".$this->db->escape($deviceId);
			$result = $this->db->query ( $query );
				
			if ($result->num_rows () > 0) {
				//update
				$query = "UPDATE device_info SET fcmId=".$this->db->escape($fcmRegId). ", deviceType=".$this->db->escape($deviceType).", modificationDate=now() WHERE deviceId=".$this->db->escape($deviceId);
				$result = $this->db->query ( $query );
				if($result){
					return true;
				}else{
					return false;
				}
			} else {
				//insert
				$query = "INSERT INTO device_info (`deviceId`, `deviceType`, `fcmId`, `user_id`)  VALUES (".$this->db->escape($deviceId).",". $this->db->escape($deviceType).",". $this->db->escape($fcmRegId).",". $this->db->escape($userId).")";
				$result = $this->db->query ( $query );
				if($result){
					return true;
				}else{
					return false;
				}
			}
	}
	
	/**
	 * @author : Anjani Kumar Gupta
	 * Date: 12th Nov 2016
	 * Method: addLeads
	 * Description: add leads
	 */
	public function addLeads($data){
		$response = new Response ();
		try {
			$query = "INSERT INTO leads (`name`, `mobile`, `category_id`, `product_id`, `cluster`, `branch`, `sourcedBy`, `address`, `emailId`, `isExistingCustomer`, `cutomerId`, `businessCard`, `added_by`, `status`, `creationDate`)  
					VALUES (".$this->db->escape($data['name']).",". $this->db->escape($data['mobile']).",". $this->db->escape($data['category_id']).",". $this->db->escape($data['product_id']).",". $this->db->escape($data['cluster']).",
							". $this->db->escape($data['branch']).",". $this->db->escape($data['sourcedBy']).",". $this->db->escape($data['address']).",". $this->db->escape($data['emailId']).",". $this->db->escape($data['isExistingCustomer']).",". $this->db->escape($data['cutomerId']).",". $this->db->escape($data['businessCard']).",". $this->db->escape($data['added_by']).",". $this->db->escape($data['status']).",now())";
			/* print_r($query); die(); */
			$result = $this->db->query ( $query );
				
			if ($result) {
				$response->setStatus ( 1 );
				$response->setMsg ( "Lead added." );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "Error in adding lead." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
		
	}
	
	/**
	 * @author : Anjani Kumar Gupta
	 * Date: 12th Nov 2016
	 * Method: getCategory
	 * Description: get category
	 */
	public function getCategory(){
			$response = new Response ();
			$query = "SELECT * FROM m_category WHERE status=".ACTIVE;
			$result = $this->db->query ( $query );
				
			if ($result->num_rows () > 0) {
				$response->setStatus ( 1 );
				$response->setMsg ( "Category found." );
				$response->setObjArray ( $result->result() );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "No category found." );
				$response->setObjArray ( $result->result() );
			}
			
			return $response;
	}
	
	/**
	 * @author : Anjani Kumar Gupta
	 * Date: 12th Nov 2016
	 * Method: getProduct
	 * Description: get product
	 */
	public function getProduct($catId){
		$response = new Response ();
		$query = "SELECT * FROM category_products WHERE category_id=".$this->db->escape($catId)." AND status=".ACTIVE;
		$result = $this->db->query ( $query );
	
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "Category found." );
			$response->setObjArray ( $result->result() );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "No category found." );
			$response->setObjArray ( $result->result() );
		}
			
		return $response;
	}
	
	/**
	 * @author : Anjani Kumar Gupta
	 * Date: 12th Nov 2016
	 * Method: getCluster
	 * Description: get cluster
	 */
	public function getCluster(){
		$response = new Response ();
		$query = "SELECT id,title FROM m_branch WHERE parent IS NULL AND status=".ACTIVE;
		$result = $this->db->query ( $query );
	
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "Cluster found." );
			$response->setObjArray ( $result->result() );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "No cluster found." );
			$response->setObjArray ( $result->result() );
		}
			
		return $response;
	}
	
	/**
	 * @author : Anjani Kumar Gupta
	 * Date: 12th Nov 2016
	 * Method: getBranch
	 * Description: get branch
	 */
	public function getBranch($clusterId){
		$response = new Response ();
		$query = "SELECT id,title FROM m_branch WHERE parent IS NOT NULL AND parent=".$this->db->escape($clusterId)." AND status=".ACTIVE;
		$result = $this->db->query ( $query );
	
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "Branch found." );
			$response->setObjArray ( $result->result() );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "No branch found." );
			$response->setObjArray ( $result->result() );
		}
			
		return $response;
	}
	/**
	 * @author : Nishant Singh
	 * Date: 21th Nov 2016
	 * Method: getStatus
	 * Description: get all status
	 */
	public function getStatus(){
		$response = new Response ();
		$query="SELECT * from m_status";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "here is all status." );
			$response->setObjArray ( $result->result() );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "here is all status." );
			$response->setObjArray ( $result->result() );
		}
			
		return $response;
	}
	/**
	 * @author : Nishant Singh
	 * Date: 21th Nov 2016
	 * Method: changeStatus
	 * Description: get branch
	 */
	public function changeStatus($data){
		$response = new Response ();
		$query = "UPDATE leads SET status=". $this->db->escape($data['statusId'])." WHERE id=".$this->db->escape($data['leadId']);
		$result = $this->db->query ( $query );
	
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "Status successfully changed." );
			$response->setObjArray ( $result->result() );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "There is some problem in updating status......" );
			$response->setObjArray ( $result->result() );
		}
			
		return $response;
	}
	/**
	 * @author : Nishant Singh
	 * Date: 21th Nov 2016
	 * Method: addMeeting
	 * Description: add meeting
	 */
	public function addMeeting($data){
		$response = new Response ();
		try {
			$query = "INSERT INTO meetings (`title`, `meetingTime`, `description`,`user_id`,`status`,`creationDate`)
					VALUES (".$this->db->escape($data['title']).",". $this->db->escape($data['meetingTime']).",". $this->db->escape($data['description']).",". $this->db->escape($data['user_id']).",". $this->db->escape($data['status']).",now())";
			$result = $this->db->query ( $query );
			if ($result) {
				$response->setStatus ( 1 );
				$response->setMsg ( "Meeting  added." );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "Error in adding Meeting." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	
	}
	
	/**
	 * @author : Nishant Singh
	 * Date: 21th Nov 2016
	 * Method: getAllMeeting
	 * Description: get all meeting
	 */
	public function getAllMeeting($data){
		$response = new Response ();
		$query="SELECT * from meetings where user_id=".$data['userId'];
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setObjArray ( $result->result() );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "something went wrong" );
			$response->setObjArray ( $result->result() );
		}
			
		return $response;
	}
	/**
	 * @author : Nishant Singh
	 * Date: 22th Nov 2016
	 * Method: getLeadsSummary
	 * Description:  Here we get how many leads are available and how many leads are processed
	 */
	public function getLeadsSummary()
	{
		$response = new response();
		$query="SELECT (SELECT count(*) FROM leads) as total_counts, m_status.title , COUNT( * ) as count FROM leads
                INNER JOIN m_status ON m_status.id = leads.status
				where leadS.status=".CONTACTED." or leads.status=".QUALIFIED." or leads.status=".DEMO." or leads.status=".PROPOSAL."
                GROUP BY leads.status";  
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "how many loeads are available." );
			$response->setObjArray ( $result->result() );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result() );
		}
			
		return $response;
}
/**
 * @author : Nishant Singh
 * Date: 22th Nov 2016
 * Method: getProcessedSummary
 * Description:  Here we get how many leads are processed
 */
public function getProcessedSummary($data)
{
	$response=new response();
	$query="SELECT (SELECT count(*) FROM leads) as total_counts, m_status.title , COUNT( * ) as count FROM leads
            INNER JOIN m_status ON m_status.id = leads.status
			where leadS.status=".CLOSED." or leads.status=".CANCELLED." GROUP BY leads.status";
	$result = $this->db->query ( $query );
	if ($result->num_rows () > 0) {
		$response->setStatus ( 1 );
		$response->setMsg ( "total processed lead." );
		$response->setObjArray ( $result->result() );
	} else {
		$response->setStatus ( 0 );
		$response->setMsg ( "somethins going wrong" );
		$response->setObjArray ( $result->result() );
	}
		
	return $response;
}
/**
 * @author : Nishant Singh
 * Date: 23th Nov 2016
 * Method: updateLeadDetails
 * Description: Edit or Modify the lead details
 */
public function updateLeadDetails($data,$id){
	$response = new Response ();
	try {
		$this->db->set('modificationDate', date('Y-m-d H:i:s',now()));
		$this->db-> where('id',$id);
		$result=$this->db-> update('leads',$data);	
		if ($result) {
			$response->setStatus ( 1 );
			$response->setMsg ( "data updated successfully" );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "Error in upadating data........." );
		}
	} catch ( Exception $e ) {
		$response->setStatus ( - 1 );
		$response->setMsg ( $e->getMessage () );
		$response->setError ( $e->getMessage () );
		log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
	}
	return $response;
}
/**
 * @author : Nishant Singh
 * Date: 24th Nov 2016
 * Method: updateUserDetails
 * Description: update meeting details
 */
public function updateUserDetails($data){
	$response = new Response ();
	try {
		$this->db->set('modificationDate', date('Y-m-d H:i:s',now()));
		$this->db-> where('id',$data['id']);
		$result=$this->db-> update('user',$data);
	
		if ($result) {
			$response->setStatus ( 1 );
			$response->setMsg ( "data updated successfully" );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "Error in upadating data........." );
		}
	} catch ( Exception $e ) {
		$response->setStatus ( - 1 );
		$response->setMsg ( $e->getMessage () );
		$response->setError ( $e->getMessage () );
		log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
	}
	return $response;
}
/**
 * @author : Nishant Singh
 * Date: 24th Nov 2016
 * Method: updateMeetingDetails
 * Description: update meeting details
 */
public function updateMeetingDetails($data,$id){

	$response = new Response ();
	try {
		$this->db->set('modificationDate', date('Y-m-d H:i:s',now()));
		$this->db-> where('id',$id);
		$result=$this->db-> update('meetings',$data);

		if ($result) {
			$response->setStatus ( 1 );
			$response->setMsg ( "data updated successfully" );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "Error in upadating data........." );
		}
	} catch ( Exception $e ) {
		$response->setStatus ( - 1 );
		$response->setMsg ( $e->getMessage () );
		$response->setError ( $e->getMessage () );
		log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
	}
	return $response;
}
/**
 * @author : Nishant singh
 * @method : changePassword
 * @Desc   :  update password
 * @Date   :   24th Nov 2016
 **/
public function changePassword($confirmPassword,$email) {
	try {
	 $response = new Response ();
		$password=md5($confirmPassword);
	 $query = "UPDATE user SET password=".$this->db->escape($password).", modificationDate=now() WHERE email=".$this->db->escape($email);
	 $result = $this->db->query ( $query );						
if ($result) {
		$response->setStatus ( 1 );
			
		} else {
			 $response->setStatus ( 0 );
		}
	}catch ( Exception $e ) {
		$response->setStatus ( - 1 );
		$response->setMsg ( $e->getMessage () );
		$response->setError ( $e->getMessage () );
		log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
	}
	return $response;
}
/**
 * @author : Nishant Singh
 * Date: 25th Nov 2016
 * Method: getCategorywiseUserLeads
 * Description: get all categorywise user individual leads
 */
public function getCategorywiseUserLeads($data)
{ 
	$response = new response();
	$query="SELECT  m_category.category , COUNT( * ) as count
            FROM leads
            INNER JOIN m_category ON m_category.id = leads.category_id where leads.added_by=".$data['userId']." and leads.category_id=1 or leads.added_by=".$data['userId']." and leads.category_id=2 or leads.added_by=".$data['userId']." and leads.category_id=3 or leads.added_by=".$data['userId']." and leads.category_id=4 
            GROUP BY leads.category_id";
	$result = $this->db->query ( $query );
	if ($result->num_rows () > 0) {
		$response->setStatus ( 1 );
		
		$response->setObjArray ( $result->result() );
	} else {
		$response->setStatus ( 0 );
		$response->setMsg ( "somethins going wrong" );
		$response->setObjArray ( $result->result() );
	}
		
	return $response;
}
/**
 * @author : Nishant Singh
 * Date: 25th Nov 2016
 * Method: getProductwiseUserLeads
 * Description: get all productwise user individual leads
 */
public function getProductwiseUserLeads($data)
{
	$response = new response();
	$query="SELECT  category_products.product , COUNT( * ) as count
            FROM leads
           INNER JOIN category_products ON category_products.id = leads.product_id where leads.added_by=".$data['userId']." and leads.product_id=1 or leads.added_by=".$data['userId']." and leads.product_id=2 or leads.added_by=".$data['userId']." and leads.product_id=3 or leads.added_by=".$data['userId']." and leads.product_id=4
           or leads.added_by=".$data['userId']." and leads.product_id=5 or leads.added_by=".$data['userId']." and leads.product_id=6 or leads.added_by=".$data['userId']." and leads.product_id=3 or leads.added_by=".$data['userId']." and leads.product_id=4 
           or leads.added_by=".$data['userId']." and leads.product_id=9 or leads.added_by=".$data['userId']." and leads.product_id=10 or leads.added_by=".$data['userId']." and leads.product_id=11 or leads.added_by=".$data['userId']." and leads.product_id=12	
           or leads.added_by=".$data['userId']." and leads.product_id=13 or leads.added_by=".$data['userId']." and leads.product_id=14 or leads.added_by=".$data['userId']." and leads.product_id=15 or leads.added_by=".$data['userId']." and leads.product_id=16	
  			GROUP BY leads.product_id";
	$result = $this->db->query ( $query );
	if ($result->num_rows () > 0) {
		$response->setStatus ( 1 );

		$response->setObjArray ( $result->result() );
	} else {
		$response->setStatus ( 0 );
		$response->setMsg ( "somethins going wrong" );
		$response->setObjArray ( $result->result() );
	}

	return $response;
}
/**
 * @author : Nishant Singh
 * Date: 25th Nov 2016
 * Method: getLeaderboardCatageorywiseLeads
 * Description: Leaderboard categorywise leads
 */
public function getLeaderboardCatageorywiseLeads($data)
{
	$response = new response();
	$query = "select count(*) as count,u.firstname FROM user u INNER JOIN leads l ON u.id=l.added_by WHERE l.category_id=".$data['catId'] ." GROUP BY l.added_by
    order by count desc";
	$result = $this->db->query ( $query );
	if ($result->num_rows () > 0) {
		$response->setStatus ( 1 );

		$response->setObjArray ( $result->result() );
	} else {
		$response->setStatus ( 0 );
		$response->setMsg ( "somethins going wrong" );
		$response->setObjArray ( $result->result() );
	}

	return $response;
}
/**
 * @author : Nishant Singh
 * Date: 25th Nov 2016
 * Method: getLeaderboardCategorywiseValues
 * Description: Leaderboard categorywise value 
 */
public function getLeaderboardCategorywiseValues($data)
{
	$response = new response();
	$query = "select sum(leadValue) as sum_value,u.firstname FROM user u INNER JOIN leads l ON u.id=l.added_by WHERE l.category_id=".$data['catId'] ." GROUP BY l.added_by
    order by sum_value desc";
	$result = $this->db->query ( $query );
	if ($result->num_rows () > 0) {
		$response->setStatus ( 1 );

		$response->setObjArray ( $result->result() );
	} else {
		$response->setStatus ( 0 );
		$response->setMsg ( "somethins going wrong" );
		$response->setObjArray ( $result->result() );
	}

	return $response;
}
}
?>