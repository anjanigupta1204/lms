<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : API management service
 * this service class is responsible of all the application logic 
 * related to API
 */

 class Api_service extends CI_Model
 {
	 public function __construct() {
            parent::__construct();
            $this->load->model('api/Api_dao');
            include_once './application/objects/Response.php';
      }

      /**
       * @author : Anjani Kumar Gupta
       * Date: 10th Nov 2016
       * Method: login
       * Description: validate credentials
       */
	
		public function login($email,$password,$deviceType,$deviceId,$fcmRegId)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$isUserExist = $this->isUserExist($email);
				if($isUserExist->getStatus()==1){
					
					$user = $isUserExist->getObjArray()[0];
					if($user->status==ACTIVE){
						if(md5($password)==$user->password){
							 $updateDeviceData = $apiDao->updateDeviceData($user->id,$deviceType,$deviceId,$fcmRegId);
							 if($updateDeviceData){
							 	$response->setStatus ( 1 );
							 	$response->setMsg ( "Valid user" );
							 	$response->setObjArray ( $user );
							 }else {
							 	$response->setStatus ( 0 );
							 	$response->setMsg ( "Database error" );
							 	$response->setObjArray ( NULL );
							 }
						}else {
							$response->setStatus ( 0 );
							$response->setMsg ( "Invalid credentials" );
							$response->setObjArray ( NULL );
						}
					}else {
						$response->setStatus ( 2 );
						$response->setMsg ( "Inactive user" );
						$response->setObjArray ( NULL );
					}
				}else{
					$response = $isUserExist;
				}
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		public function isUserExist($email)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->isUserExist($email);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: addLeads
		 * Description: add leads
		 */
		public function addLeads($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->addLeads($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getCategory
		 * Description: get category
		 */
		public function getCategory(){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getCategory();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getProduct
		 * Description: get product
		 */
		public function getProduct($catId){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getProduct($catId);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getCluster
		 * Description: get cluster
		 */
		public function getCluster(){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getCluster();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		/**
		 * @author : Anjani Kumar Gupta
		 * Date: 12th Nov 2016
		 * Method: getBranch
		 * Description: get branch
		 */
		public function getBranch($clusterId){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getBranch($clusterId);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: getStatus
		 * Description:  All Status shown here
		 */
		public function getStatus(){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getStatus();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: changeStatus
		 * Description: Change Status
		 */
		public function changeStatus($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->changeStatus($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: addMeeting
		 * Description: add meeting
		 */
		public function addMeeting($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->addMeeting($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
			
		/**
		 * @author : Nishant Singh
		 * Date: 21th Nov 2016
		 * Method: getAllMeeting
		 * Description:  All meeting of a sales person shown here
		 */
		public function getAllMeeting($data){
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getAllMeeting($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 22th Nov 2016
		 * Method: getLeadsSummary
		 * Description:  Here we get how many leads are available 
		 */
		public function getLeadsSummary()
		{
			$response=new $response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getLeadsSummary();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 22th Nov 2016
		 * Method: getProcessedSummary
		 * Description:  how many leads are processed
		 */
		public function getProcessedSummary()
		{
			$response=new response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getProcessedSummary();
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		
		/**
		 * @author : Nishant Singh
		 * Date: 22th Nov 2016
		 * Method: picture()
		 * Description: adding image
		 */
		public function picture()
		{
			$config['upload_path']   = './uploads';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']      = 1024;
			$config['max_width']     = 1024;
			$config['max_height']    = 768;
			$this->load->library('upload', $config);
			$sizeImage=getimagesize($_FILES['businessCard']['tmp_name']);
			$width=$sizeImage[0];
			$height=$sizeImage[1];
			if($width > 1024 && $height > 768)
			{
				
				echo 'you upload size greater than allowed value';
                 unlink($_FILES['businessCard']['tmp_name']);
			}elseif
			 ( ! $this->upload->do_upload('businessCard')) {
		
				//$error = array('error' => $this->upload->display_errors());
				//echo "somethning is going wrong";
			}
			 
			else {
				$data = array('upload_data' => $this->upload->data());
				return $data;
			}
		}
		/**
		 * @author : Nishant Singh
		 * Date: 23th Nov 2016
		 * Method: updateLeadDetails
		 * Description: update details leads
		 */
		public function updateLeadDetails($data,$id)
		{ 
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->updateLeadDetails($data,$id);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 24th Nov 2016
		 * Method: updateMeetingDetails
		 * Description: update meeting details
		 */
		public function updateMeetingDetails($data,$id)
		{  
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->updateMeetingDetails($data,$id);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 24th Nov 2016
		 * Method: updateUserDetails
		 * Description: update User Details
		 */
		public function updateUserDetails($data)
		{ //print_r($data); die();
		$response = new Response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->updateUserDetails($data);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
		}
		/**
		 * @author : nishant singh
		 * Date: 24th Nov 2016
		 * Method: chaqgePassword
		 * Description: validate credentials
		 */
		
		public function changePassword($password,$email,$newPassword,$confirmPassword)
		{
			$response = new Response();
			try {
				$apiDao = new Api_dao();
				$isUserExist = $this->isUserExist($email);
				if($isUserExist->getStatus()==1){	
					$user = $isUserExist->getObjArray()[0];
						if(md5($password)==$user->password){
						$updatePassword = $apiDao->changePassword($confirmPassword,$email);
							if($updatePassword){
								$response->setStatus ( 1 );
								$response->setMsg ( "successfully password update" );
								$response->setObjArray ( $user );
							}else {
								$response->setStatus ( 0 );
								$response->setMsg ( "Database error" );
								$response->setObjArray ( NULL );
							}
						}else {
							$response->setStatus ( 0 );
							$response->setMsg ( "Invalid credentials" );
							$response->setObjArray ( NULL );
						}
				}else{
					$response = $isUserExist;
				}
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 25th Nov 2016
		 * Method: getCategorywiseUserLeads
		 * Description:  get all catageorywise user individual leads
		 */
		public function getCategorywiseUserLeads($data)
		{
			$response=new response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getCategorywiseUserLeads($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 25th Nov 2016
		 * Method: getProductwiseUserValues
		 * Description:get all productwise user individual leads
		 */
		public function getProductwiseUserLeads($data)
		{
			$response=new response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getProductwiseUserLeads($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 25th Nov 2016
		 * Method: getLeaderboardCatageorywiseLeads
		 * Description:  Leaderboard categorywise
		 */
		public function getLeaderboardCatageorywiseLeads($data)
		{
			$response=new response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getLeaderboardCatageorywiseLeads($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
		/**
		 * @author : Nishant Singh
		 * Date: 25th Nov 2016
		 * Method: getLeaderboardCategorywiseValues
		 * Description:  Leaderboard productwise 
		 */
		public function getLeaderboardCategorywiseValues($data)
		{
			$response=new response();
			try {
				$apiDao = new Api_dao();
				$response = $apiDao->getLeaderboardCategorywiseValues($data);
			} catch (Exception $e) {
				$response->setStatus(-1);
				$response->setMsg($e->getMessage());
				$response->setError($e->getMessage());
				log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
			}
			return $response;
		}
 }
 ?>