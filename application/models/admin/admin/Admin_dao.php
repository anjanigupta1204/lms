<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Admin_dao extends CI_Model {
	
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
	}
	
	public function checkEmail($email)
	{
		$this->db->where('email',$email);
		$result = $this->db->get('user');
		$rows = $result->num_rows ();
		if($rows === 1)
		{
			return true;
		}
			
	}
	
	public function updatePassword ( $random_code, $email )
	{
		$data['password'] = md5($random_code);
		$this->db->where('email',$email);
		$update = $this->db->update ('user', $data );
			
		if($update)
		{
			return true;
		}
			
		else
		{
			return false;
		}
	}
	
	public function GetUserProfile($userid)
	{
		$this->db->where('id',$userid);
		$result = $this->db->get('user');
		$rows = $result->row();
		return $rows;
	}
	
	public function update_profile($data,$userid)
	{
		$this->db->where('id',$userid);
		$result = $this->db->update('user',$data);
		if($result)
		{
			return true;
		}
			
			
	}
	
	public function update_password($data,$userid)
	{
		$this->db->where('id',$userid);
		$result = $this->db->update('user',$data);
		if($result)
		{
			return true;
		}
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getRoles
	 * Description:  get Roles
	 */
	public function getRoles()
	{
		$this->db->select('id,title,description,status');
		$query = $this->db->get('m_roles');
		$result= $query->result_array();
		return $result;
		//print_r($result); die();
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getPrivileges
	 * Description:  get Privileges
	 */
	public function getPrivileges()
	{    $this->db->select('id,title,description,status');
	$query = $this->db->get('m_privileges');
	$result= $query->result_array();
	return $result;
	/* print_r($result); die(); */
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getRoles
	 * Description:  get Roles
	 */
	public function editRoles($id)
	
	{
		$this->db->where('id',$id);
		$this->db->select('id,title,description,status');
		$query = $this->db->get('m_roles');
		$rows = $query->row();
		return $rows;
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getPrivileges
	 * Description:  get Privileges
	 */
	public function editPrivileges($id)
	{    
		$this->db->where('id',$id);
		$this->db->select('id,title,description,status');
	$query = $this->db->get('m_privileges');
	$rows = $query->row();
	return $rows;
	/* print_r($result); die(); */
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getPrivileges
	 * Description:  get Privileges
	 */
	public function update_role($data)
	{
		$this->db->where('id',$data['id']);
		$result = $this->db->update('m_roles',$data);
		if($result)
		{
			return true;
		}
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getPrivileges
	 * Description:  get Privileges
	 */
	public function update_privilege($data)
	{
		$this->db->where('id',$data['id']);
		$result = $this->db->update('m_privileges',$data);
		if($result)
		{
			return true;
		}
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getPrivileges
	 * Description:  get Privileges
	 */
	public function new_role($data)
	{
		$this->db->insert('m_roles',$data);
		return true;
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: getPrivileges
	 * Description:  get Privileges
	 */
	public function new_privilege($data)
	{
		$this->db->insert('m_privileges',$data);
		return true;
	}
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: new user
	 * Description: new  user
	 */
	public function new_user($data)
	{
		$userId = NULL;
		$user = $this->db->insert('user',$data);
		if($user){
			$userId = $this->db->insert_id();
			//$this->add_image($userId);
		}
		
		return $userId;
	}
	 
	/**
	 * @author : Nishant Singh
	 * Date: 26th Nov 2016
	 * Method: new user
	 * Description: new  user
	 */
	 public function getUserDetails()
	{
		$this->db->select('firstname,lastname,email,gender,dob,primaryContact,city,role_id,status,creationDate');
		$query = $this->db->get('user');
	$result= $query->result_array();
	//print_r($result); die();
	return $result;
	} 
	public function addImage($filename,$description)
	{   
		//print_r($_SESSION);
		$sql="INSERT INTO gallery(`picture`,`description`,`added_by`) VALUES(".$this->db->escape($filename).",".$this->db->escape($description).",305)";
		//print_r($sql); die();
		$result = $this->db->query ( $sql );
			// print_r($result); die();
			return $result;
		}
	public function count()
	{
		
		 $count = $this->db->get('gallery')->num_rows();
		return $count;
	}
	public function getImage($limit,$start)
	{
	
		$this->db->select('picture,description');
		$this->db->limit($limit, $start);
		$this->db->order_by('creationDate','desc');
		$query = $this->db->get('gallery');
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data ; 
        }
        return false;
	}
}