<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
         <?php $this->view("admin/side_menu"); ?>
        </div>
		 <?php $this->view("admin/top_nav"); ?>
				<!-- page content -->

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>View image</h2>
									<div class="clearfix"></div>
								</div>
								<button type="button" class="btn btn-info btn-lg"
									data-toggle="modal" data-target="#myModal">Upload Image</button>

								<!-- Modal -->
								<div id="myModal" class="modal fade" role="dialog">
									<div class="modal-dialog">

										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Select New Image</h4>
												<p><?php //echo $this->session->flashdata('msg'); ?></p>
											</div>
											<div class="modal-body">
											<div class="row">
								<div class="col-md-3 col-sm-6 col-xs-12">
											
									<form enctype="multipart/form-data" action="" method="post" id="image">
											<div class="form-group">
												<label>description</label> <input type="text"
													class="form-control" name="description" id="description" />
											</div>
											<div class="form-group">
												<label>Choose Files</label> <input type="file"
													class="form-control" name="fileToUpload"  id="fileToUpload" />
											</div>
											<div class="form-group">
												<input class="form-control" type="button" name="fileSubmit" id="submit"
													value="UPLOAD" />
											</div>
									</form>
											</div>
											</div>
											<!-- <div class="modal-footer">
												<button type="button" class="btn btn-default"
													data-dismiss="modal">Close</button>
											</div> -->
										</div>

									</div>
								</div>
								</div>
								<div class="row">
									 
                    <?php      if(!empty($files)): foreach($files as $file): ?>
                   
                    <div class="col-sm-3">
										<img width="100"
											src="<?php echo base_url('uploads/files/'.$file->picture); ?>"
											
											alt="" class="img-responsive">
											 <?php echo $file->description; ?>
									</div>
											
                    <?php endforeach; ?>
                   
                    <?php else: ?>
                   
                    <p>File(s) not found.....</p>
                    <?php endif; ?>
									</div>

								<p><?php echo $links; ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
	
	</script>

</body>