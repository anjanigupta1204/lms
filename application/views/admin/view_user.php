<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
         <?php $this->view("admin/side_menu"); ?>
        </div>
		 <?php $this->view("admin/top_nav"); ?>
				<!-- page content -->

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>View User Details</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table id="userDetails" class="display table table-striped table-bordered no-footer dataTable" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>First Name</th>
												<th>Last Name</th>
												<th>Email</th>
												<th>Gender</th>
												<th>Date Of Birth</th>
												<th>primary Contact</th>
												<th>City</th>
												<th>Role Id</th>
												<th>Status</th>
												<th>Start Date</th>
											</tr>
										</thead>
										</table>
										</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</body>