<?php //print_r($user); die(); ?>
<!-- disallow browser cache -->
<meta HTTP-EQUIV="Pragma" content="no-cache">
<meta HTTP-EQUIV="Expires" content="-1" >
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <!-- sidebar -->
         <?php $this->view("admin/side_menu"); ?>
        </div>
		 <?php $this->view("admin/top_nav"); ?>
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- page content -->
       
          <div class="">
            <div class="page-title">

            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Privileges</h2><div style="text-align:center; margin-right:15%"class="search-error">
                       
               </div> 
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form id="form" method="post" data-parsley-validate class="form-horizontal form-label-left"  action="<?php echo base_url();?>admin/view_privileges">
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="hidden" value= "<?php echo $user->id;?>" id="id" required="required" class="form-control col-md-7 col-xs-12" disabled>
                        </div>
                        </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" value= "<?php echo $user->title;?>" id="title" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" value= "<?php echo $user->description;?>"id="description" name="description" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="status" class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                       
                        <input type="radio"  class="status" name="status" value="1" <?php echo ($user->status=='1')?'checked':'' ?> >Active
                        <input type="radio" class="status" name="status" value="2" <?php echo ($user->status=='2')?'checked':'' ?> >Inactive
                          
                          </div> 
                          </div>  
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div style="text-align:center;"class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                       <!--    <button type="Button" class="btn btn-primary">Cancel</button> -->
                          <button type="button" id="privilege" class="btn btn-success">Update privilege</button>
                          <span id="loader" style=" position: relative;  top: 10px; right: 10px;"><i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i></span>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
        
      <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>assets/js/auth.js"></script>
<script src="<?php echo base_url();?>assets/js/admin.js"></script>
        <!-- /page content -->