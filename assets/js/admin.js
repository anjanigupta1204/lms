var roleObj = {};
$(document)
		.on(
				'click',
				"#role",
				function(e) {

					// alert(email);
					// console.log($("#id").val());
					roleObj.id = $.trim($("#id").val());
					roleObj.title = $.trim($("#title").val());
					roleObj.description = $.trim($("#description").val());
					roleObj.status = $.trim($(".status:checked").val());
					// console.log(roleObj.status);
					$
							.ajax({
								type : "POST",
								url : base_url + 'admin/update_role',
								beforeSend : function() {
									$('#loader').show();
								},
								data : {
									user : roleObj
								},
								dataType : "json",
								success : function(data) {

									if (data.status == 1) {
										$(".search-error").html(
												"<span style='color:green'>"
														+ data.msg + "</span>");
										window.location.href = base_url
												+ 'admin/view_roles';
										// location.reload(true);

									} else if (data.status == 0) {
										$(".search-error").html(
												"<span style='color:red'>"
														+ data.msg + "</span>");
										window.location.href = base_url
												+ 'admin/edit_role';
										// location.reload(true);

									} else {

										$(".search-error")
												.html(
														"<span style='color:red'>Server error.</span>");

									}
									// console.log(data)
									// window.location.href =
									// base_url+'admin/forgot_password';
								},
								error : function(e) {
									console.log(e)
									$('#loader').hide();
								},
								complete : function() {
									$('#loader').hide();
								}
							});
				});

var privilegeObj = {};
$(document)
		.on(
				'click',
				"#privilege",
				function(e) {

					privilegeObj.id = $.trim($("#id").val());
					privilegeObj.title = $.trim($("#title").val());
					privilegeObj.description = $.trim($("#description").val());
					privilegeObj.status = $.trim($(".status:checked").val());

					$
							.ajax({
								type : "POST",
								url : base_url + 'admin/update_privilege',
								beforeSend : function() {
									$('#loader').show();
								},
								data : {
									user : privilegeObj
								},
								dataType : "json",
								success : function(data) {

									if (data.status == 1) {
										$(".search-error").html(
												"<span style='color:green'>"
														+ data.msg + "</span>");
										window.location.href = base_url
												+ 'admin/view_privileges';

									} else if (data.status == 0) {
										$(".search-error").html(
												"<span style='color:red'>"
														+ data.msg + "</span>");
										window.location.href = base_url
												+ 'admin/edit_privileges';

									} else {

										$(".search-error")
												.html(
														"<span style='color:red'>Server error.</span>");

									}
									console.log(data)

								},
								error : function(e) {
									console.log(e)
									$('#loader').hide();
								},
								complete : function() {
									$('#loader').hide();
								}
							});
				});

var addRoleObj = {};
$(document).on(
		'click',
		"#addRole",
		function(e) {
			addRoleObj.title = $.trim($("#title").val());
			addRoleObj.description = $.trim($("#description").val());
			addRoleObj.status = $.trim($(".status:checked").val());

			$.ajax({
				type : "POST",
				url : base_url + 'admin/new_role',
				beforeSend : function() {
					$('#loader').show();
				},
				data : {
					user : addRoleObj
				},
				dataType : "json",
				success : function(data) {

					if (data.status == 1) {
						$(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
						window.location.href = base_url + 'admin/view_roles';

					} else if (data.status == 0) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");
						// location.reload(true);

					} else if (data.status == 2) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");
						// location.reload(true);

					}

				},
				error : function(e) {
					console.log(e)
					$('#loader').hide();
				},
				complete : function() {
					$('#loader').hide();
				}
			});

		});

var addprivilegeObj = {};
$(document).on(
		'click',
		"#addPrivilege",
		function(e) {
			addprivilegeObj.title = $.trim($("#title").val());
			addprivilegeObj.description = $.trim($("#description").val());
			addprivilegeObj.status = $.trim($(".status:checked").val());
			// console.log(addprivilegeObj);
			$.ajax({
				type : "POST",
				url : base_url + 'admin/new_privilege',
				beforeSend : function() {
					$('#loader').show();
				},
				data : {
					user : addprivilegeObj
				},
				dataType : "json",
				success : function(data) {

					if (data.status == 1) {
						$(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
						window.location.href = base_url
								+ 'admin/view_privileges';

					} else if (data.status == 0) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");
						// location.reload(true);

					} else if (data.status == 2) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");
						// location.reload(true);

					}
				},
				error : function(e) {
					// console.log(e)
					$('#loader').hide();
				},
				complete : function() {
					$('#loader').hide();
				}
			});

		});

var newUserObj = {};
$(document).on('click', "#register", function(e) {
	/*
	 * $("#register").validate({ rules:{ firstname: "required", lastname:
	 * "required", gender: "required", email: { required: true, email: true },
	 * picture: "required", address: "required" }, messages: { firstname: "You
	 * must enter a first name. ", lastname: "You must enter a first name.",
	 * gender: "You must enter the second line of the address.", picture: "You
	 * must enter a postal town or city. ", picture: "You must enter the postal
	 * county.", address: "You must enter the postcode. "
	 *  },
	 */
	newUserObj.firstname = $.trim($("#firstname").val());
	newUserObj.lastname = $.trim($("#lastname").val());
	newUserObj.email = $.trim($("#email").val());
	// newUserObj.picture = $('input[type=file]').val().replace(/^.*\\/, "");
	newUserObj.password = $.trim($("#password").val());
	newUserObj.gender = $.trim($(".gender:checked").val());
	newUserObj.role_id = $.trim($(".role_Id:checked").val());
	newUserObj.status = $.trim($(".status:checked").val());
	newUserObj.dob = $.trim($("#dob").val());
	newUserObj.primaryContact = $.trim($("#primaryContact").val());
	newUserObj.address = $.trim($("#address").val());
	newUserObj.pincode = $.trim($("#pincode").val());
	newUserObj.city = $.trim($("#city").val());
	newUserObj.state = $.trim($("#state").val());
	newUserObj.country = $.trim($("#city").val());
	// console.log(newUserObj);

	$.ajax({
		type : "POST",
		url : base_url + 'admin/new_user',
		beforeSend : function() {
			// $('#loader').show();
		},
		data : {
			user : newUserObj
		},
		dataType : "json",
		success : function(data) {
			console.log(data);
			if (data.status == 1) {
				// upload file
				$.ajaxFileUpload({
					url : base_url + 'admin/picture',
					secureuri : false,
					fileElementId : 'fileToUpload',
					dataType : 'JSON',
					// data:{id:newUserObj.id};
					success : function(data) {
						console.log(data)
					}
				});
			}
		},
		error : function(e) {
			console.log(e)
			$('#loader').hide();
		},
		complete : function() {
			// $('#loader').hide();
		}
	});

});

$(document)
		.on(
				'click',
				"#submit",
				function(e) {
					var description = $("#description").val();
					// description ='hello world';
					// console.log(description);

					
							$.ajaxFileUpload({
								url : base_url + 'admin/upload_image',
								secureuri : false,
								fileElementId : 'fileToUpload',
								dataType : 'json',
								data : {
									description : description
								},
								success : function(data) {
									
									if (data.status == 1) {
										if (!confirm("Image uploaded successfully. Do you want to upload another image?")) {
											$('#myModal').modal("hide");

											//alert(data.msg);
											$(".search-error").html(
													"<span style='color:green'>"
															+ data.msg
															+ "</span>");
											window.location.href = base_url
													+ 'admin/view_image';
										}
									} else if (data.status == 0) {
										$(".search-error").html(
												"<span style='color:red'>"
														+ data.msg + "</span>");
									} else {
										alert(data.msg);
										$(".search-error").html(
												"<span style='color:red'>"
														+ data.msg + "</span>");

									}
								}
							});

				});

