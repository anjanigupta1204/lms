  // initialize the validator function
//  var base_url = "http://192.168.1.20/virch/";
      validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
		
		

        if (submit)
		var Obj = {};
	    var rel = {};
        Obj.firstname = $("#firstname").val();
	    Obj.lastname = $("#lastname").val();
		Obj.gender = $("#user_gender").val();
		Obj.class = $("#classname").val();
		Obj.section = $("#section").val();
		Obj.email = $("#email").val();
		Obj.password = $("#password").val();
		Obj.rollNumber = $("#rollnumber").val();
		Obj.parentContact = $("#telephone").val();
		Obj.address = $("#address").val();
		Obj.dob = $("#birthday").val();
		//Obj.parentContact = $("#telephone").val();
		Obj.city = $("#city").val();
		Obj.pincode = $("#pin").val();
		Obj.state = $("#state").val();
		Obj.country = $("#country").val();
		Obj.emergencyContact = $("#telephone2").val();
		Obj.bloodGroup = $("#bloodgroup").val();
		Obj.summary = $("#summary").val();
		Obj.role_id = $("#role").val();;
		Obj.status = 1;
		
		rel.firstname = $("#parentname").val();
		rel.contact = $("#telephone").val();
		rel.relation_id = $("#relation").val();
		  if(Obj.role_id == 4)
		  {
			  var URL = base_url+'admin/Add_student';
		  }
		  
		   if(Obj.role_id == 3)
		  {
			  var URL = base_url+'admin/Add_teacher';
		  }
		  
		  
			$.ajax({
			  type: "POST",
			  url: URL,
			  beforeSend : function(){
				  $('#loader').show();
			  },
			  data: {user: Obj, relation: rel},
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				  if(data.status == 1){
					  
			  $('#search-error').html("<span style='color:green'>" +data.msg+"</span>");
			  $('form')[0].reset();
			  
		  }
		  else if(data.status == 0){
			  $('#search-error').html("<span style='color:red'>" +data.msg+"</span>");
			   $('form')[0].reset();
			  
		  }
		  else{
			  
			  $('#search-error').html("<span style='color:red'>Server error.</span>");
			  //$this.button('reset');
		  }
			  },
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			});
		

        return false;
      });