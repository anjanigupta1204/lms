$(document).ready(function() {
		$('#userDetails').DataTable( {
			"ajax": base_url+"admin/getUserDetails",
				"columns": [
				   
				    {"data": "firstname" },
					{"data": "lastname" },
					{"data": "email" },
					{"data": "gender" },
					{"data": "dob"},
					{"data": "primaryContact" },
					{"data": "city" },
					{"data": "status" },
					{"data": "role_id" },
					{"data": "creationDate"},
					//select:true
				],
				} ); } );
		var editor;
		$(document).ready(function() {
			editor = new $.fn.dataTable.Editor( {
        "ajax": "base_url+'admin/getPrivileges'",
        "table": "#privilegeDetails",
		"idSrc": "id",
		"fields": [
		//{ label:"Id", name:"id", search:false}	,	
		{
                "label": "Title:",
                "name": "title"
            }, {
                "label": "Description",
                "name": "description"
            }, {
                "label": "Status:",
                "name": "status",
				"type":"select",
				"options": [
                    { "label": "Active", "value": "1" },
                    { "label": "InActive", "value": "2" }
                ]
            }
        ]
    } );
 
		var table = $('#privilegeDetails').DataTable( {
			"ajax": base_url+"admin/getPrivileges",
				"columns": [
				   
				    {data: "id" },
					{data: "title" },
					{data: "description" },
					{data: "status" }
					],
					select: true
		} ); 
		     new $.fn.dataTable.Buttons( table, [
        
        { extend: "edit",   editor: editor },
        { extend: "remove", editor: editor }
    ] );
 
    table.buttons().container()
        .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
		} );
		
		
		$(document).ready(function() {
		$('#roleDetails').DataTable( {
			"ajax": base_url+"admin/getRoles",
				"columns": [
				   
				    {"data": "id" },
					{"data": "title" },
					{"data": "description" },
					{"data": "status" },
					//{"data": "action"},
					
					
				],
		} );
		} );